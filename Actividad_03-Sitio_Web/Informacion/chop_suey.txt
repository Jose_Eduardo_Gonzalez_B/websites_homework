Ingredientes:

    � lb de carne de cerdo y � lb de pechuga de pollo, previamente cocinadas y cortadas en tiras medianas
    4 ramas de apio cortadas en l�minas delgadas
    1 cucharada de jengibre rallado
    1 sobre de 6 g o 1 cucharada de sazona
    � taza de salsa de soya

    2 pimentones medianos, 2 cebollas medianas, � lb de champi�ones, 1 zanahoria, 1 calabac�n verde y 1 calabac�n amarillo: cortados en julianas
    � taza de ra�ces chinas bien lavadas
    4 cucharadas de aceite vegetal
    1 cucharada de pasta de ajo
    Sal y pimienta al gusto


Preparaci�n:

    En un wok o un sart�n bien caliente agrega el aceite y luego pon la carne de cerdo y el pollo. Despu�s disp�n los vegetales uno por uno con intervalos de 40 segundos

    Coloca la salsa soya, el jengibre, la pasta de ajo y el sazona. Salpimienta, revuelve un poco, retira del fuego y sirve.
