El pato laqueado es uno de los platos m�s representativos y apreciados de la cocina china. Es una receta originaria de Pek�n y muy t�pica en el nordeste del pa�s, adem�s de ser un plato habitual en los restaurantes chinos de todo el mundo.

El pato laqueado era el plato que com�a el emperador, por lo que era s�mbolo de dignidad y poder, antes de que se popularizara.

 Ingredientes:

    1 pato entero
    Especias al gusto: an�s estrellado, canela, hinojo, pimienta, chile rojo en polvo, jengibre fresco, etc.
    2 cucharadas de vino
    2 cucharadas de salsa de soja
    2 cucharadas de salsa Hoisin (similar a la salsa agridulce)
    1 cucharada de miel
    Para acompa�ar: cr�pes chinas, pepino y cebolla

Preparaci�n:

    Limpia bien el pato y aseg�rate de que no queden restos de v�sceras.
    Escalda el pato: pon agua a hervir en una olla grande, a�ade sal y cuando arranque la ebullici�n introduce el pato durante 1 minuto. Pasado ese tiempo, ret�ralo.
    Despega la piel del pato de la carne: cierra las aperturas con unos palillos e introduce una pajita entre la piel y la carne y sopla para que se separe sin perder la forma.
    Coloca un gancho de metal que atraviese el pato.
    Prepara una mezcla con las especias, el vino, la piel y la salsa de soja, y pinta el pato con ella. Vuelve a barnizar el pato hasta que quede de color chocolate. Deja que se seque preferiblemente colgado.
    Coc�nalo en el horno: col�calo en la rejilla y pon debajo una fuente con agua. �salo durante unos 20 minutos a 220 �C y despu�s baja la temperatura a 180-150 �C durante 1 hora, o hasta que se vea crujiente.
    Limpia y corta las verduras en bastones.
    Cuando saques el pato de horno, c�rtalo en peque�as porciones y s�rvelo con las cr�pes chinas y los bastones de verduras.
