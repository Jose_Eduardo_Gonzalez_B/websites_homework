la sopa de wonton, un plato muy popular de la gastronom�a china. El wonton es una especie de ravioli chino, normalmente relleno de carne picada de cerdo o de gambas, que se puede consumir o fritos (como aperitivo) o hervidos y servidos en una sopa (como primer plato). La palabra wonton (o wantan) parte de la transcripci�n fon�tica en canton�s ?? y significa literalmente �tragando nubes�, que es la apariencia que adquieren estos wonton una vez hervidos y flotando en la sopa.

 Al parecer el wonton se origin� en Guangzhou y fue despu�s de la Segunda Guerra Mundial cuando se hizo popular a trav�s de Hong Kong. Es curioso que hasta el segundo periodo de la Dinast�a Song (1127 � 1279) el wonton era considerado un manjar para ricos. Hoy d�a el wonton ha traspasado fronteras no s�lo dentro del continente asi�tico en pa�ses como Filipinas (pinsec frito), Indonesia (pangsit goreng) y Tailandia, sino que tambi�n ha llegado a Per�, Estados Unidos y Canad� de la mano de emigrantes chinos.

 A lo largo de la geograf�a china el wonton var�a en forma, tama�o y en ingredientes. En mandar�n se dice h�nt�n ?? que significa caos: H�nt�n simb�licamente imita la armon�a en la creaci�n del mundo y la cultura del ser humano. En China es tradici�n comer wonton en  el und�cimo mes del calendario lunar, periodo que coincide con el solsticio de invierno y comienza aproximadamente el 22 de diciembre y dura doce d�as. Nos gustar�a seguir indagando en la conexi�n entre el wonton y el cosmos, as� como el trasfondo simb�lico del wonton dentro de la tradici�n china, pero ya empezamos a tener hambre*, as� que vamos a dar paso a Magdalena con una receta sencilla de sopa de wonton.
 Sopa Wonton. Los ingredientes:

Pasta wontoon hecha

O bien hacerla:
2 vasos de harina
1 huevo
1 cucharada peque�a de sal
1/2 vaso de agua

El relleno:
300 gr de carne picada de cerdo
100gr de gambas picadas
1 clara de huevo
2 cucharadas de salsa de ostras
1 cucharada peque�a de harina de maizena
1/2 vaso de hojas de cebolla tierna picada

El caldo:
3 dientes de ajos troceados
1/2 cucharada peque�a de jengibre troceado
2 litros de caldo de pollo
1 cuharada peque�a de sal
1 cucharada de aceite de s�samo

*acelgas, hojas extra de cebolla, cebolla frita
 Sopa Wonton. Paso a paso:

Hacer la masa de wontoon:
1. Mezclar la harina, el huevo, la sal y el agua. Amasar durante 20 minutos. 2. Dejarla en reposo durante 1 hora.
3. Aplanar la masa y cortarla a cuadros de 5x5cm. Utilizar la harina de maizena para enharinar la pasta para que no se quede pegada.

Envolver el wonton

Hacer el relleno:
Mezclar bien la carne picada de cerdo, las gambas, la salsa de ostras, la clara de huevo, la harina de maizena y los cortes de hojas de cebolla tierna.

Hacer el wontoon:
Rellenar 1 cucharada peque�a en la pasta de wontoon, cerrarla para se quede con forma de tri�ngulo. Coger los 2 lados, a izquierda y derecha y plegarlos hacia abajo y engancharlos juntos.
Para cocinarlo, hervirlo durante unos minutos hasta que la pasta de quede blanda.

Si quieres congelar los wontoon, ponerlos separados en una bandeja en el congelador durante 15 minutos. Los wontoons ya no se quedar�n pegados y ahora los puedes meter en una bolsa de pl�stico para congelarlos.
Si quieres guardarlas en el frigo, t�palas con una pa�o de cocina h�medo en un recipiente cerrado. Aguantar�n 1-2 d�as.

Sopa Wonton

Hacer la sopa:
En una olla, saltear el ajo y el jengibre, en cuanto este�n dorados, a�adir el caldo de pollo y la sal. Dejarlo hervir durante unos minutos.
Apagar el fuego y a�adir el aceite de s�samo.

Para servir:
Poner las acelgas(se cocinar�n con el calor de caldo), el wontoon, el caldo, los cortes de cebolla frita y la cebolla frita.
La sopa de wontoon est� lista para tomar?!!