El Zong o Zongzi, es un plato muy t�pico en la cocina tradicional china a base de arroz glutinoso con diferentes rellenos y enrollado con hojas de bamb�. Es un plato que se ha extendido bastante, pues lo cierto es que en otros lugares como Tailandia, Camboya o Vietnam tambi�n cuentan con alguna variante.

Desde el punto de vista tradicional, este plato es servido durante el Festival del barco drag�n, una fiesta celebrada aproximadamente durante la primera quincena de junio. Durante esta fiesta, se intenta honrar la figura de Qu Yuan. Este hombre era un famoso poeta chino que vivi� durante los Reinos Combatientes. Intent� proteger a su rey de los avances de los hombres de Qin, no obstante, un general consigui� agarrarlo y tirarlo al r�o. Seg�n la leyenda, los habitantes del lugar tiraban rellenos de arroz para que los peces no terminaran comi�ndose el cuerpo del poeta. Otra versi�n asegura que el arroz buscaba calmar al drag�n que habitaba esas aguas. Sea como sea, todos los a�os se come este plato tradicional durante esta fiesta para conmemorar a este valiente hombre.

No podemos negar que se trata de una receta compleja, pues aunque a simple vista parezca no entra�ar complicaciones, lo cierto es que el enrollado es una t�cnica que pasa de generaci�n en generaci�n. Aun as� podemos aventurarnos e intentar hacerlos en casa.

INGREDIENTES

    Arroz glutinoso redondo: 1.5 kg � 2 kg.
    Hojas de bamb� disecado: 50 hojas.
    Hongos shitake mini disecado.
    Bondiola estofada: 1 kg.
    Salsa de soja: 50 ml.
    Aceite de girasol: 30 ml.
    Man� con piel: 300 g.
    Hilo de matambre.

PREPARACI�N

Para realizar este plato tardaremos 3 d�as. Durante 48-72 horas tendremos que dejar las hojas de bamb� en remojo.

El segundo d�a procederemos a cocinar la bondiola estofada, dej�ndola reposar en una heladera durante unas 24 horas. Es imprescindible que la carne est� completamente tierna.

Debemos adem�s lavar el arroz con mucha agua y ponerlo en remojo durante 24 horas tambi�n. Asimismo, tanto los man�es como los hongos de shitake deber�n estar en remojo el mismo tiempo.

El tercer d�a procederemos a lavar bien las hojas de bamb� y las escurriremos correctamente. Tambi�n escurriremos los hongos y el resto de alimentos.

Salteamos el arroz con un poco de aceite y salsa de soja y le a�adimos un poquito de sal al gusto.

Debemos cortar los hilos con una longitud de unos 60 cm- 100 cm. Usaremos 2 hojas de bamb� para formar con ellas un cono, las rellenamos de arroz, carne, hongos y arroz y lo cerramos. Acto seguido intentamos armar un tamal con 4 �ngulos y lo atamos con el hilo.

Tras esto, debemos cocinarlos en agua o al vapor durante aproximadamente 1 hora o 1 hora y media.